/**
   Hinzufügen der Bibliotheken
*/
#include <Adafruit_NeoPixel.h>
#include <LiquidCrystal_I2C.h> //Display
#include <Wire.h>              //Uhr  
#include <Keypad.h>            //Bibliothek zum auslesen der Matrix Tastatur
#include "RTClib.h"            //RTC Uhr


//Definieren der Zeilen und Spalten.
//Wenn eine größere Tastatur verwendet wird,
//muss das hierdefiniert werden.


/**
   Initialisierung des Displays und der Uhr
*/
LiquidCrystal_I2C lcd(0x27, 16, 2);
RTC_DS1307 RTC;
Adafruit_NeoPixel led(12, 10, NEO_GRB + NEO_KHZ800);

const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
char KEYS[ROWS][COLS] = {
  {'4', '5', '6'},
  {'1', '2', '3'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};
byte ROW_PINS[ROWS] = {9, 8, 7, 6}; //connect to the row pinouts of the keypad
byte COL_PINS[COLS] = {5, 4, 3}; //connect to the column pinouts of the keypad
Keypad myKeypad = Keypad(makeKeymap(KEYS), ROW_PINS, COL_PINS, ROWS, COLS); //Init Keypad

/**
 * Initialisierung der globalen Variablen. Diese sind überall im Programmcode verfügbar, nicht nur innerhalb der Funktionen
 */
int messwert = 0;
const int PIN_BUZZ = 11; //Mit dem Namen "PIN_BUZ" wird der Pin10 bezeichnet, an dem ein Piezo-Speaker angeschlossen wird.
DateTime clock_start;
TimeSpan sec_from_start;
DateTime last_action;
int last_r, last_g, last_b;

/**
   Deklaration der Funktionen

   Definitionen der Funktionen sind alle unterhalb der Loop
*/
void print_Time(DateTime time, short line); //Funktion zur Ausgabe der Uhrzeit
void print_Date(DateTime time, short line); //Funktion zur Ausgabe des Datums
void stoppwatch(); //Funktion Stoppuhr
void alert(DateTime time); //Funktion Alarm
void timer(TimeSpan span); //Funktion Timer
DateTime enter_Time(); //Funktion Uhrzeit eingeben
TimeSpan enter_Span(); //Funktion Zeitspanne eingeben
void all_led(int r, int g, int b);
void measure_rain(int messwert);


void setup()
{
  lcd.init(); //LCD Display init
  lcd.backlight(); //LCD-Display Backlight an 
  led.begin(); //LED-Ring an
  noTone(PIN_BUZZ);

  pinMode (PIN_BUZZ, OUTPUT); //Im Setup wird der Pin11 als Ausgang festgelegt. -> Buzzer

  RTC.begin(); //Initialisierung der Uhr
  if (! RTC.isrunning()) { //Wenn die Uhr NICHT läuft, stelle die Uhr
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  clock_start = RTC.now();
  last_action = RTC.now();
  sec_from_start = TimeSpan(0);
}

void loop()
{
  DateTime now = RTC.now(); //Hole aktuelle Uhrzeit
  print_Time(now, 0); //Datumsausgabe
  print_Date(now, 1);
  char key = myKeypad.getKey(); //Warte auf Eingabe
  if (key){ //Wenn Eingabe
    /**
     Umsetzung des Struktogramms "Setup"
    */
    if (key == '1') {
      all_led(0, 0, 155); //Blau Stoppuhr
      lcd.clear();
      stoppwatch();
    }

    if (key == '2') {
      lcd.clear();
      all_led(255, 0, 0); //Rot Alarm
      DateTime alertTime = enter_Time();
      alert(alertTime);
    }

    if (key == '3') {
      lcd.clear();
      all_led(222, 255, 111); //Orange Timer
      TimeSpan t = enter_Span();
      timer(t);
    }
    last_action = RTC.now();
  }
  
  if (((last_action.secondstime()) + 15 ) < now.secondstime()){ //Ist der Sleeptimeout inzwischen in der Vergangenheit? Wenn ja Display aus und Helllila
    lcd.noBacklight();
    all_led(55, 0, 155); //Helllila Timeout
  }
  else{ //Wenn nicht Display an und Grün
    lcd.backlight();
    all_led(0, 155, 0);  
  }

  /**
   * Regenmessung
   */
  int messwert = analogRead(A0);
  measure_rain(messwert);
}

/**
 * Funktion zur Ausgabe der Eingegeben Uhrzeit
 * @Param: time Eine Uhrzeit
 *         line Zeile zur Ausgabe
 * @return void Druckt in Display
 */
void print_Time(DateTime time, short line)
{
  char time_formatted[16]; //Initialisiere String
  sprintf(time_formatted, "%02d:%02d:%02d", time.hour(), time.minute(), time.second()); //Baue Uhrzeitstring zusammen
  lcd.setCursor(0, line); //Setze den Cursor auf das Feld
  lcd.print(time_formatted); //Gebe Uhrzeit auf LCD-Display aus
}

/**
 * Funktion zur Ausgabe des Eingegeben Datums
 * @Param: time Ein Datum
 *         line Zeile zur Ausgabe
 * @return void Druckt in Display
 */
void print_Date(DateTime time, short line)
{
  char date_formatted[16]; //Initialisiere String
  sprintf(date_formatted, "%02d.%02d.%02d", time.day(), time.month(), time.year()); //Baue Datumsstring zusammen
  lcd.setCursor(0, line); //Setze den Cursor auf das Feld
  lcd.print(date_formatted); //Gebe Datum auf LCD-Display aus
}

/**
 * Funktion zum Ablauf und Ausgabe der Stoppuhr
 * @Param: null
 * @return void Druckt in Display
 */
void stoppwatch()
{
  short run = 1, clock_run = 0; // clock_run auf 1 startet die Stoppuhr, run auf 0 kehrt zum Hauptmenü zurück
  DateTime start, end; //Start und Endzeitpunkte zur Berechnung der verstrichenen Zeit

  char span_formatted[16] = "00:00:00"; //Ausgabestring Startzeitpunkt
  lcd.print(span_formatted);

  while (run) {//Solange die Taste # nicht gedrückt wurde, verharrt das Programm in dieser Endlosschleife
    lcd.setCursor(0, 0);
    char key = myKeypad.getKey(); //Warte auf Tastatureingabe
    if (key == '0') { //Starte die Stoppuhr
      start = RTC.now(); //Speichere den Startzeitpunkt in der Variable start
      clock_run = 1;
    }
    if (clock_run) {
      end = RTC.now(); //Speichere den jetzigen Zeitpunkt in der Variable end
      TimeSpan span = TimeSpan(end.secondstime() - start.secondstime()); //Verstrichene Zeit = jetztger Zeitpunkt - Startzeitpunkt, Speichere in TimeSpan
      sprintf(span_formatted, "%02d:%02d:%02d", span.hours(), span.minutes(), span.seconds()); //C kennt keine Strings, Char Array anstatt dessen
      lcd.print(span_formatted);
    }
    if (key == '*') {
      clock_run = 0;// Stoppe die Stoppuhr, aber kehre nicht zum Hauptmenü zurück
    }
    if (key == '#') //Abbruch bei Taste rechts unten
      run  = 0;
  }
}

/**
 * Weckerfunktion
 * @Param: time Die Weckzeit
 * @return void Druckt in Display
 */
void alert(DateTime time)
{
  char key;
  short run = 1;
  lcd.clear();
  print_Time(time, 1);
  while (run) {//Solange die Taste # nicht gedrückt wurde, verharrt das Programm in dieser Endlosschleife
    DateTime now = RTC.now(); //Speichere die aktuelle hrzeit in der Variable now
    print_Time(now, 0);
    if (time.secondstime() <= now.secondstime()) { //Weckerzeit wird mit aktueller Zeit vergleichen: Ist die Weckerzeit kleiner als die aktuelle Zeit, aktiviere das Wecksignal
      tone(PIN_BUZZ, 100);
      delay(5000);
      noTone(PIN_BUZZ);
      run = 0;
    }
    key = myKeypad.getKey();
    if (key == '#') //Abbruch bei Taste rechts unten
      run = 0;
  }
  lcd.clear();
}

/**
 * Timerfunktion
 * @Param: span Zeitspanne wie lange der Timer laufen soll
 * @return void Druckt in Display
 */
void timer(TimeSpan span)
{
  char key;
  short run = 1;
  DateTime end = RTC.now();
  end = end + span; //Der Timer ist abgelaufen wenn der Zeitpunkt jetzt + Zeitspanne überschritten wurde
  while (run) {//Solange die Taste # nicht gedrückt wurde, verharrt das Programm in dieser Endlosschleife
    key = myKeypad.getKey();
    char span_formatted[16];
    DateTime now = RTC.now();
    TimeSpan ts = end - now;//Die verbleibende Zeitspanne berechnet sich aus der vorherberechneten Endzeit - dem jetztigen Zeitpunkt
    sprintf(span_formatted, "%02d:%02d:%02d", ts.hours(), ts.minutes(), ts.seconds());
    lcd.setCursor(0, 0);
    lcd.print(span_formatted);
    if (ts.totalseconds() == 0) {
      tone(PIN_BUZZ, 100);
      delay(5000);
      noTone(PIN_BUZZ);
      run = 0;
    }
    if (key == '#') //Abbruch bei Taste rechts unten
      run = 0;
  }
}

/**
 * Funktion zur Eingabe der Zeit
 * @Param: null
 * @return DateTime ein Datum/Zeit Objekt
 */
DateTime enter_Time()
{
  short in = 0;
  short size = 6;
  char numbers[size]; //Array: Feld aus zusammenhängenden Daten
  char time[8]; //Ersatzstring
  DateTime dt;
  while (in < size) {//Gebe 6 Zeichen ein: hh:mm:ss
    char number = myKeypad.getKey();
    if (number) {
      lcd.print(number);
      numbers[in] = number;
      in ++;// Ein weiteres Zeichen wurde eingegeben
    }
  }
  sprintf(time, "%c%c:%c%c:%c%c", numbers[0], numbers[1], numbers[2], numbers[3], numbers[4], numbers[5]); //Drucke die Eingegebene Zeit formatiert
  dt = DateTime(__DATE__, time); // Erzeuge Rückgabeobjekt
  lcd.clear();
  print_Time(dt, 0);
  return dt; //Gebe Datum/Zeit Objekt zurück
}

/**
 * Funktion zur Eingabe einer Zeitspanne
 * @Param: null
 * @return TimeSpan ein Datum/Zeit Objekt
 */
TimeSpan enter_Span()
{
  short in = 0;
  int numbers[6];
  TimeSpan ts;
  while (in < 6) {//Gebe 6 Zeichen ein: hh:mm:ss
    char number = myKeypad.getKey();
    if (number) {
      lcd.print(number);
      numbers[in] = ((int)number - 48);
      in++;// Ein weiteres Zeichen wurde eingegeben
    }
  }
  ts = TimeSpan(0, ((numbers[0] * 10) + numbers[1]), ((numbers[2] * 10) + numbers[3]), ((numbers[4] * 10) + numbers[5]));//Erzeuge Rückgabeobjekt
  return ts; //Gebe Zeitspane zurück
}

/**
 * Funktion zur Farbeneinstellung und aktivierung aller LED's
 * @Param: r roter  Farbanteil
 *         g grüner Farbanteil
 *         b blauer Farbanteil
 * @return void LED Farben
 */
void all_led(int r, int g, int b)
{
  if (r != last_r || g != last_g || b != last_b){
    last_r = r;
    last_g = g;
    last_b = b;
    for (int i = 0; i < 12; i++){
      led.setPixelColor(i, led.Color(last_r, last_g, last_b)); // Pixel1 leuchtet in der Farbe Grün
    }
    led.show();
  }
}

/**
 * Funktion zur Regenbestimmung in Sekundenintervallen
 * @Param: messwert Messwert des Regensensors
 * @return void LED Farben
 */
void measure_rain(int messwert)
{
    TimeSpan One = TimeSpan(1);//Speichere Zeitspanne von einer Sekunde in der Variable One ab
    char val[4];
    DateTime now = RTC.now();
    TimeSpan ts = TimeSpan(now.secondstime() - clock_start.secondstime()); //Ermittle die Zeitspanne seit der letzten Messung
    if (ts.totalseconds() > sec_from_start.totalseconds()){//Ist mindestens eine Sekunde seit der letzten Messung vergangen tue folgendes
        sprintf(val, "%4d", messwert);//Erstelle MEssungsstring
        //lcd.setCursor(0, 1); //Setze den Cursor auf das obere linke Feld
        //lcd.print(val); Messwert auf Display ausgeben- diese und obrige Zeile wieder entkommentieren
        sec_from_start = sec_from_start + One;//Aktualisiere den Zeitpunkt der letzten Messung
        if (messwert > 200){//Ist der Messwert größer als 200 wurde der Sensor nass
          tone(PIN_BUZZ, 100);  
        }
        else{
         noTone(PIN_BUZZ);  
       }
    }
}
